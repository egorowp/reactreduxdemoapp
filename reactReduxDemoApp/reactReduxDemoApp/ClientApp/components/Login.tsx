import * as React from 'react';
import { Link, RouteComponentProps } from 'react-router-dom';
import { connect } from 'react-redux';
import { ApplicationState }  from '../store';
import * as LoginStore from '../store/Login';
import { Button, FormGroup, FormControl, ControlLabel } from "react-bootstrap";


type LoginProps =
    LoginStore.LoginState
    & typeof LoginStore.actionCreators
    & RouteComponentProps<{}>;

export class Login extends React.Component<{}, {}> {
        state: { email: string;password: string }
    constructor(props:any) {
        super(props);

        this.state = {
            email: "",
            password: ""
        };
    }
    validateForm() {
        return this.state.email.length > 0 && this.state.password.length > 0;
    }
    handleChange = (event: any) => {
        this.setState({
            [event.target.id]: event.target.value
        });
    }
    handleSubmit = (event: any) => {
        event.preventDefault();
    }
    public render() {
        return (
            <div className="Login">
                <form onSubmit={this.handleSubmit}>
                    <FormGroup controlId="email" bsSize="large">
                        <ControlLabel>Email</ControlLabel>
                        <FormControl
                            autoFocus
                            type="email"
                            value={this.state.email}
                            onChange={this.handleChange}
                        />
                    </FormGroup>
                    <FormGroup controlId="password" bsSize="large">
                        <ControlLabel>Password</ControlLabel>
                        <FormControl
                            value={this.state.password}
                            onChange={this.handleChange}
                            type="password"
                        />
                    </FormGroup>
                    <Button
                        block
                        bsSize="large"
                        disabled={!this.validateForm()}
                        type="submit"
                    >
                        Login
                    </Button>
                </form>
            </div>
        );
    }
}

