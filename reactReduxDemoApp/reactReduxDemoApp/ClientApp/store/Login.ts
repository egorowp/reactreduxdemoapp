import { Action, Reducer } from 'redux';

export interface LoginState {
    isLoggined: boolean;
    login: string;
    pswd: string;
}

interface LoginAction { type: 'LOGIN' }
interface LogoutAction { type: 'LOGOUT' }

type KnownAction = LoginAction | LogoutAction;

// ----------------

export const actionCreators = {
    login: () => <LoginAction>{ type: 'LOGIN' },
    logout: () => <LogoutAction>{ type: 'LOGOUT' }
};


export const reducer: (state: LoginState, action: KnownAction) => Object = (state: LoginState, action: KnownAction) => {
    switch (action.type) {
        case 'LOGIN':
            return { isLoggined: state.isLoggined = true };
        case 'LOGOUT':
            return { isLoggined:  state.isLoggined = false };
        default:
            // The following line guarantees that every action in the KnownAction union has been covered by a case above
            const exhaustiveCheck: never = action;
    }

    // For unrecognized actions (or in cases where actions have no effect), must return the existing state
    //  (or default initial state if none was supplied)
    return state || { isLoggined: true };
};
